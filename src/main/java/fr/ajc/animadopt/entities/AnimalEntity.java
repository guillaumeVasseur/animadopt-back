package fr.ajc.animadopt.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Animal")
public class AnimalEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idAnimal;
	@Column(name = "espece")
	private String espece;
	@Column(name = "nom")
	private String nom;
	@Column(name = "description", columnDefinition = "VARCHAR(2000)")
	private String description;
	@Column(name = "image")
	private String image;
	@Column(name = "url")
	private String url;
	@Column(name = "age")
	private String age;
	@Column(name = "sexe")
	private String sexe;
	@Column(name = "sterile")
	private String sterile;
	@Column(name = "race")
	private String race;
	@Column(name = "naissance")
	private String naissance;
	@Column(name = "tatouage")
	private String tatouage;
	@Column(name = "region")
	private String region;
	@Column(name = "histoire", columnDefinition = "VARCHAR(2000)")
	private String histoire;
	@Column(name = "caractere", columnDefinition = "VARCHAR(2000)")
	private String caractere;
	@Column(name = "commentaire", columnDefinition = "VARCHAR(2000)")
	private String commentaire;
	@Column(name = "refuge")
	private String refuge;

	public AnimalEntity() {
	}

	public String getEspece() {
		return espece;
	}

	public void setEspece(String espece) {
		this.espece = espece;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if (description.length() > 2000) {
			this.description = description.substring(0, 2000);
		} else {
			this.description = description;
		}
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getSterile() {
		return sterile;
	}

	public void setSterile(String sterile) {
		this.sterile = sterile;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getNaissance() {
		return naissance;
	}

	public void setNaissance(String naissance) {
		this.naissance = naissance;
	}

	public String getTatouage() {
		return tatouage;
	}

	public void setTatouage(String tatouage) {
		this.tatouage = tatouage;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getHistoire() {
		return histoire;
	}

	public void setHistoire(String histoire) {
		if (histoire.length() > 2000) {
			this.histoire = histoire.substring(0, 2000);
		} else {
			this.histoire = histoire;
		}
	}

	public String getCaractere() {
		return caractere;
	}

	public void setCaractere(String caractere) {
		if (caractere.length() > 2000) {
			this.caractere = caractere.substring(0, 2000);
		} else {
			this.caractere = caractere;
		}
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		if (commentaire.length() > 2000) {
			this.commentaire = commentaire.substring(0, 2000);
		} else {
			this.commentaire = commentaire;
		}
	}

	public String getRefuge() {
		return refuge;
	}

	public void setRefuge(String refuge) {
		this.refuge = refuge;
	}

	@Override
	public String toString() {
		return "Animal [espece=" + espece + ", nom=" + nom + ", description=" + description + ", image=" + image
				+ ", url=" + url + ", age=" + age + ", sexe=" + sexe + ", sterile=" + sterile + ", race=" + race
				+ ", naissance=" + naissance + ", tatouage=" + tatouage + ", region=" + region + ", histoire="
				+ histoire + ", caractere=" + caractere + ", commentaire=" + commentaire + ", refuge=" + refuge + "]";
	}

}