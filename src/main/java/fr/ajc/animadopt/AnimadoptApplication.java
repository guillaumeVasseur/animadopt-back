package fr.ajc.animadopt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import fr.ajc.animadopt.service.WebScrapper;

@SpringBootApplication
public class AnimadoptApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnimadoptApplication.class, args);

		WebScrapper scrapper = new WebScrapper();
		try {
			scrapper.scrap30MillionsAmis();
			// scrapper.scrapSpa();
		} catch (Exception e) {
			// TODO à logger
			System.out.println("Une erreur est survenue");
			e.printStackTrace();
		}
	}

}
