package fr.ajc.animadopt.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import fr.ajc.animadopt.entities.AnimalEntity;
import fr.ajc.animadopt.util.Context;

public class AnimalDao {

	/** Boucle sur la liste d'animaux et les insèrent tous en BDD **/
	public void createAnimal(List<AnimalEntity> animalList) {

		EntityManagerFactory emf = Context.getInstance().getEmf(); // crée la route
		EntityManager em = emf.createEntityManager(); // créer un livreur
		em.getTransaction().begin();
		for (AnimalEntity animal : animalList) {
			em.persist(animal);
		}
		em.getTransaction().commit();
		em.close();
	}

}
